<?php

require('animal.php');
require('frog.php');
require('ape.php');


$animal = new Animal("Shaun");
echo "Name : $animal->name <br>";
echo "Legs : $animal->legs <br>";
echo "Cold_blooded : $animal->cold_blooded <br><br>";


$frog = new Frog("Buduk");
echo "Name : $frog->name <br>";
echo "Legs  : $frog->legs <br>";
echo "Cold_blooded : $frog->cold_blooded<br>";
echo $frog->jump(). '<br><br>';

$ape = new Ape("Sungokong");
echo "Name : $ape->name <br>";
echo "Legs  : $ape->legs <br>";
echo "Cold_blooded : $ape->cold_blooded <br>";
echo $ape->yell(). '<br>';


?>